<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnquiryFollowUp extends Model
{
    //
    protected $fillable = [ 'enquiry_id', 'comment' ];
    protected $hidden = ['created_at', 'updated_at'];
}
