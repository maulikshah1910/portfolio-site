<?php

namespace App\Http\Controllers\Admin;

use App\Enquiry;
use App\EnquiryFollowUp;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    //
    function index(Request $request){
        $today = Carbon::today()->format('Y-m-d');
        // get today's enquiry count
        $enqCount = Enquiry::where('created_at', 'LIKE', $today . '%')->count();
        // get today's followup count
        $followUpCount = EnquiryFollowUp::where('next_followup_on', $today)->count();

        return view('admin.dashboard', compact('followUpCount', 'enqCount'));
    }
    function logout(Request $request){
        Auth::logout();
        return redirect('/login');
    }
    function chartData(Request $request){
        try {
            $fromDate = $request->get('from');
            $toDate = $request->get('to');

            $dateRange = CarbonPeriod::create($fromDate, $toDate)->toArray();
            $chartDates = [];

            foreach ($dateRange as $dates) {
                $chartDates[$dates->format('Y-m-d')] = 0;
            }

            $enquiries = DB::select(DB::raw(" SELECT DATE(created_at) as enq_date, count(*) as enq_count 
                                    FROM ENQUIRIES
                                    GROUP BY enq_date
                                    HAVING enq_date BETWEEN '$fromDate' AND '$toDate' 
                                    ORDER BY enq_date ASC "));

            foreach ($enquiries as $e) {
                $chartDates[$e->enq_date] = $e->enq_count;
            }

            $labels = array_keys($chartDates);
            $chartData = array_values($chartDates);

            return response()->json(['success'=>true, 'labels' => $labels, 'data' => $chartData], 200);
        } catch (\Exception $ex){
            return response()->json(['success'=>false, 'error'=>$ex->getMessage()], 200);
        }
    }
}
