<?php

namespace App\Http\Controllers\Admin;

use App\Enquiry;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class EnquiryController extends Controller
{
    //
    function index(Request $request){
        return view('admin.enquiry');
    }
    function enquiryData(Request $request){
        $table = DataTables::of(Enquiry::all());

        $table->addColumn('action', function($enquiry){
            $action = '';
//            $action .= '<button class="btn btn-xs btn-warning btn-enquiry-view" data-enq="' . $enquiry->id . '"><i class="fa fa-eye"></i></button> ';
            $action .= '<button class="btn btn-xs btn-success btn-followup" data-enq="'.$enquiry->id.'"><i class="fa fa-at"></i></button>';
            return $action;
        })->editColumn('created_at', function($enquiry){
            $data = $enquiry->created_at;
            $date = date('d-M-Y h:i A', strtotime($data));
            return $date;
        })->addColumn('ep', function($enquiry){
            $mail = $enquiry->email;
            $phone = $enquiry->phone;

            return $mail . ' ' . $phone;
        });

        $table->rawColumns(['action']);

        return $table->make(true);
    }
    function enquiryInfo(Request $request){
        $eid = $request->get('enq');
        try{
            $enquiry = Enquiry::where('id', $eid)->first();
            if ($enquiry){
                return response()->json(['success'=>true, 'data'=>$enquiry], 200);
            } else {
                return response()->json(['success'=>false, 'reason'=>'not_found'], 200);
            }
        } catch (\Exception $ex){
            return response()->json(['success'=>false, 'reason'=>$ex->getMessage(), 200]);
        }
    }
    function submitFollowUp(Request $request){
        return response()->json($request->all(), 200);
    }
}
