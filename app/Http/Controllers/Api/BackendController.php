<?php

namespace App\Http\Controllers\Api;

use App\Enquiry;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BackendController extends Controller
{
    //
    function submitEnquiry(Request $request){
        $enquiry = new Enquiry();
        $enquiry->name = $request->get('name');
        $enquiry->email = $request->get('email');
        $enquiry->phone = $request->get('phone');
        $enquiry->subject = $request->get('subject');
        $enquiry->message = $request->get('message');

        try {
            $enquiry->save();
            return response()->json(['success'=>true], 200);
        } catch (\Exception $ex){
            return response()->json(['success'=>false, 'reason'=>$ex->getMessage()], 200);
        }
    }
}
