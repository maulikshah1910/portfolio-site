<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Maulik Shah',
                'email' => 'maulik.shah1910@gmail.com',
                'password' => bcrypt('maulik'),
            ]
        ];
        DB::table('users')->insert($users);
    }
}
