/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


import Vue from 'vue'
import VueRouter from 'vue-router'
import JQuery from 'jquery'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { KinesisContainer, KinesisElement } from 'vue-kinesis'
import VueKinesis from 'vue-kinesis'
import axios from 'axios'


Vue.use(VueRouter)
Vue.use(axios)


Vue.component('kinesis-container', KinesisContainer)
Vue.component('kinesis-element', KinesisElement)

import SiteHeader from './components/header.vue'
import SiteFooter from './components/footer.vue'
import Home from './views/home.vue'
import About from './views/about.vue'
import Contact from './views/contact.vue'
import NotFound from './views/not-found.vue'


const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home
        },
        {
            path: '/about',
            name: 'About',
            component: About
        },
        {
            path: '/contact',
            name: 'Contact',
            component: Contact
        },
        {
            path: '/404',
            name: 'NotFound',
            component: NotFound
        },
        {
            path: '*',
            component: NotFound
        }
    ],
});
const app = new Vue({
    el: '#appContent',
    data: {},
    components: {
        SiteHeader,
        SiteFooter,
    },
    router,
});