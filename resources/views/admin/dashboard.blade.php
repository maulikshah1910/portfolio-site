@extends('layouts.admin-layout')
@section('title', 'Application Dashboard')

@section('page_styles')
    <style>
        #myChart {
            max-width: 100%;
            max-height: 450px;
        }
    </style>
@endsection

@section('page_scripts')
    <script src="{{ asset('js/plugins/chartJs/Chart.min.js') }}"></script>
    <script>
        $.ajaxSetup( {
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        } );
        function drawChart(labels, data){
            var bg = [];
            var border = [];

            for (i = 0; i < labels.length; i++){
                bg.push('#333');
                border.push('#625635');
            }
            $('#chartDiv').html('');
            $('#chartDiv').append('<canvas id="myChart"></canvas>');
            var canvas = document.getElementById('myChart');
            var chart = new Chart(canvas, {
                type: "bar",
                data: {
                    labels: labels,
                    datasets: [{
                        label: "# of Enquiries",
                        data: data,
                        backgroundColor: bg,
                        borderColor: border,
                        borderWidth: 1,
                    }]
                }
            });

            chart.update();
        }
        function getDateRange(chartType){
            var start = "";
            var end = "";
            var d = new Date();
            switch(parseInt(chartType)){
                case 1:
                    // for weekly selection, get date From Monday to Sunday
                    var monday = getMonday(d);
                    var m1 = "0" + (monday.getMonth()+1);
                    m1 = m1.slice('-2');

                    var sunday = new Date();
                    var s1 = "0" + (sunday.getMonth()+1);
                    s1 = s1.slice('-2');
                    sunday.setDate(monday.getDate() + 6);

                    start = monday.getFullYear() + "-" + m1+ "-" + ("0" + monday.getDate()).slice('-2');
                    end = sunday.getFullYear() + "-" + s1 + "-" + ("0" + sunday.getDate()).slice('-2');
                    break;
                case 2:
                    // for monthly selection, get date from First Day of Month to Last Day of month
                    // get current month
                    var month = d.getMonth();
                    var daysOfMonth = new Date(d.getFullYear(), month+1, 0).getDate();

                    var m1 = ("0" + (month+1)).slice(-2);
                    start = d.getFullYear() + "-" + m1 + "-" + "01";
                    end =  d.getFullYear() + "-" + m1 + "-" + daysOfMonth;
                    break;
                case 3:
                    // for yearly selection, get date from First January to 31st December
                    // get current year
                    var year = d.getFullYear();
                    start = year + "-01-01";
                    end =  year + "-12-31";
                    break;
            }

            return {
                from: start,
                to: end
            };
        }
        function getMonday(d) {
            d = new Date(d);
            var day = d.getDay(),
                diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
            return new Date(d.setDate(diff));
        }

        $(document).on('change', '#chartFilter', function(){
            var val = $(this).val();
            var range = getDateRange(val);
            console.log(range);
            $.ajax({
                type: 'POST',
                url: '{{ url('dashboard/chartdata') }}',
                data: {
                    from: range.from,
                    to: range.to
                },
                success: function(response){
                    if (response.success == true){
                        var chartLabels = response.labels;
                        var chartData = response.data;

                        drawChart(chartLabels, chartData);
                    }
                    console.log(response);
                }
            });
        });
    </script>
@endsection

@section('page_content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Dashboard</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">
                    <strong>Dashboard</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-3">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Today's Enquiries</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{$enqCount}}</h1>
                        <div class="stat-percent font-bold text-success"><a href="{{ url('/enquiry') }}">View Enquiries <i class="fa fa-long-arrow-right"></i></a></div>
                        <small>Number of enquiries made today</small>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Today's Follow Ups</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{$followUpCount}}</h1>
                        <div class="stat-percent font-bold text-success"><a href="{{ url('/enquiry') }}">View Enquiries <i class="fa fa-long-arrow-right"></i></a></div>
                        <small>Number of follow ups to be made today</small>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6 text-right">
                                <select name="" id="chartFilter">
                                    <option value="1" selected>Weekly</option>
                                    <option value="2">Monthly</option>
                                    <option value="3">Yearly</option>
                                </select>
                            </div>
                        </div>
                        <div class="flot-chart-content" id="chartDiv">
                            <canvas id="myChart"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection