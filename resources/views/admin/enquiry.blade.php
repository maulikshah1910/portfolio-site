@extends('layouts.admin-layout')
@section('title', 'Website Enquiry')

@section('page_styles')
    <link rel="stylesheet" href="{{ asset('css/plugins/dataTables/datatables.min.css') }}" />
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet" />
@endsection

@section('page_scripts')
    <script src="{{ asset('js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                radioClass: 'iradio_square-green',
            });
        });
        $.ajaxSetup( {
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        } );
        $('#tblEnquiries').dataTable({
            processing: true,
            serverSide: true,
            type: 'POST',
            ajax: {
                type: 'POST',
                url: '{{ url('enquiry/data') }}'
            },
            columns: [
                { data: 'id', searchable: false, visible: false },
                { data: 'created_at' },
                { data: 'name' },
                { data: 'email' },
                { data: 'phone', sortable: false },
                { data: 'subject' },
                { data: 'action', class: 'text-right', searchable: false, sortable: false },
            ],
            initComplete: function(){
                this.api().columns([2,3,4,5]).every(function(){
                    var col = this;
                    var colHeader = col.header();
                    var colTitle = $(colHeader).html();
                    var colIndex = $(colHeader).index();
                    var colName = col.name;

                    var input = $('<input type="text" class="form-control" name="'+colName+'" id="field-'+colIndex+'" placeholder="'+colTitle+'" />')
                        .appendTo( $(col.footer()).empty() )
                        .on("change", function(){
                            var val = $(this).val();
                            col.search( val ? val : "", true, false).draw();
                        });
                });
            }

        });

        $(document).on('click', '.btn-enquiry-view', function(){
            var enqID = $(this).attr('data-enq');
            $('.enqInfo').addClass('d-none');
            $('.enqError').addClass('d-none');
            $.ajax({
                type: 'POST',
                url: '{{ url('enquiry/info') }}',
                data: {
                    enq: enqID
                },
                success: function(response){
                    console.log(response);
                    if (response.success == true){
                        var data = response.data;
                        $('#infoName').html(data.name);
                        $('#infoEmail').html(data.email);
                        $('#infoPhone').html(data.phone);
                        $('#infoSubject').html(data.subject);
                        $('#infoMessage').html(data.message);

                        $('.enqInfo').removeClass('d-none');
                        $('.enqError').addClass('d-none');
                    } else {
                        var error = response.reason;
                        var errMsg = '';
                        switch(error){
                            case 'not_found': errMsg = 'Unfortunately, enquiry is not available...'; break;
                            default: errMsg = error; break;
                        }
                        $('#errorMessage').html(errMsg);
                        $('.enqInfo').addClass('d-none');
                        $('.enqError').removeClass('d-none');
                    }
                    $('#modal-enquiry-info').modal('show');
                },
                error: function (error){
                    var errMsg = error.statusText;
                    $('#errorMessage').html(errMsg);
                    $('.enqInfo').addClass('d-none');
                    $('.enqError').removeClass('d-none');
                    $('#modal-enquiry-info').modal('show');
                    console.log(error);
                }
            });
        });
        $(document).on('click', '.btn-followup', function(){
            var enqID = $(this).attr('data-enq');
            $.ajax({
                type: 'POST',
                url: '{{ url('enquiry/info') }}',
                data: {
                    enq: enqID
                },
                success: function(response){
                    console.log(response);
                    if (response.success == true){
                        var data = response.data;
                        $('#enqID').val(data.id);
                        $('#efName').html(data.name);
                        $('#efEmail').html(data.email);
                        $('#efPhone').html(data.phone);
                        $('#efSubject').html(data.subject);
                        $('#efMessage').html(data.message);

                        $('#modalEnqFollowUp').modal('show');
                    } else {
                        var error = response.reason;
                        var errMsg = '';
                        switch(error){
                            case 'not_found': errMsg = 'Unfortunately, enquiry is not available...'; break;
                            default: errMsg = error; break;
                        }
                        $('#errorMessage').html(errMsg);
                        $('.enqInfo').addClass('d-none');
                        $('.enqError').removeClass('d-none');
                        $('#modal-enquiry-info').modal('show');
                    }
                },
                error: function (error){
                    var errMsg = error.statusText;
                    $('#errorMessage').html(errMsg);
                    $('.enqInfo').addClass('d-none');
                    $('.enqError').removeClass('d-none');
                    $('#modal-enquiry-info').modal('show');
                    console.log(error);
                }
            });
        });
        $(document).on('submit', '#frmFollowUp', function(e){
            e.preventDefault();
            alert('submit');
        })
    </script>
@endsection

@section('page_content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Received Enquiries</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ url('dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Website Enquiries</strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Enquiries Received</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-hover table-bordered" id="tblEnquiries">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Enquiry Date</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Subject</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal inmodal" id="modal-enquiry-info" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content animated flipInX">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal">
                        <i class="fa fa-times-circle-o"></i>
                    </button>
                    <h4 class="modal-title">Enquiry Info</h4>
                </div>
                <div class="modal-body">
                    <div class="row enqInfo">
                        <div class="col-md-12">
                            <div class="row my-3">
                                <div class="col-sm-4"><strong>Name</strong></div>
                                <div class="col-sm-8"><label id="infoName"></label></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-sm-4"><strong>Email</strong></div>
                                <div class="col-sm-8"><label id="infoEmail"></label></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-sm-4"><strong>Phone Number</strong></div>
                                <div class="col-sm-8"><label id="infoPhone"></label></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-sm-4"><strong>Subject</strong></div>
                                <div class="col-sm-8"><label id="infoSubject"></label></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-sm-4"><strong>Message</strong></div>
                                <div class="col-sm-8"><label id="infoMessage"></label></div>
                            </div>
                        </div>
                    </div>
                    <div class="row enqError">
                        <div class="col-md-12">
                            <div class="alert alert-warning">
                                <h3>Error...!</h3>
                                <span id="errorMessage"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal inmodal" id="modalEnqFollowUp" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInUp">
                <div class="modal-header">
                    <button class="close" data-dismiss="modal"><i class="fa fa-times-circle-o"></i></button>
                    <h4 class="modal-title">Follow-Up</h4>
                </div>
                <div class="modal-body">
                    <div class="row" id="rowEnqInfo">
                        <div class="col-md-12">
                            <div class="row my-3">
                                <div class="col-md-4"><strong>Name:</strong></div>
                                <div class="col-md-8"><span id="efName"></span></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-4"><strong>Email:</strong></div>
                                <div class="col-md-8"><span id="efEmail"></span></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-4"><strong>Phone:</strong></div>
                                <div class="col-md-8"><span id="efPhone"></span></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-4"><strong>Subject:</strong></div>
                                <div class="col-md-8"><span id="efSubject"></span></div>
                            </div>
                            <div class="row my-3">
                                <div class="col-md-4"><strong>Message:</strong></div>
                                <div class="col-md-8"><span id="efMessage"></span></div>
                            </div>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="row" id="rowFollowUpForm">
                        <div class="col-md-12">
                            <form action="" method="post" class="form-horizontal" id="frmFollowUp">
                                {{ csrf_field() }}
                                <input type="hidden" name="enqID" id="enqID" value="" />
                                <div class="row form-group">
                                    <label for="" class="col-form-label col-md-3">Comment: </label>
                                    <div class="col-md-9">
                                        <textarea type="text" name="followup_comment" id="comment" class="form-control" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label for="" class="col-md-3 col-form-label">Finished?</label>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="i-checks"><label> <input type="radio" value="0" name="enq_finish" checked /> <i></i> No</label></div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="i-checks"><label> <input type="radio" value="1" name="enq_finish" /> <i></i> Yes </label></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="row form-group">
                                    <div class="col-md-12 text-right">
                                        <button class="btn btn-white btn-sm" type="button" data-dismiss="modal">Cancel</button>
                                        <button class="btn btn-primary btn-sm" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection