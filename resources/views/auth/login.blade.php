<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login - {{ config('app.name') }}</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">MS</h1>
        </div>
        <h3>Maulik Shah Website Portal</h3>
        <p>Login in. To see it in action.</p>
        <form class="m-t" role="form" method="post" action="{{ route('login') }}">
            @csrf
            @if ($errors->has('email'))
                <div class="form-group">
                    <label class="label label-danger">{{$errors->first()}}</label>
                </div>
            @endif
            <div class="form-group">
                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="User Name" />
            </div>
            <div class="form-group">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password" />
            </div>
            <button type="submit" class="btn btn-primary">
                {{ __('Login') }}
            </button>
        </form>
        <p class="m-t"> <small>&copy; {{ now()->year }} Maulik Shah</small> </p>
        <p class="m-t"><a class="btn btn-default" href="{{ url('/') }}"><i class="fa fa-long-arrow-left"></i> Back to site</a> </p>
    </div>
</div>

<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
</body>
</html>