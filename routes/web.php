<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Auth::routes();
Route::group(['middleware' => ['auth'], 'namespace' => 'Admin'], function (){
    Route::get('dashboard', 'DashboardController@index');
    Route::get('logout', 'DashboardController@logout');

    Route::get('enquiry', 'EnquiryController@index');
    Route::post('enquiry/data', 'EnquiryController@enquiryData');
    Route::post('enquiry/info', 'EnquiryController@enquiryInfo');
    Route::post('enquiry/follow-up', 'EnquiryController@submitFollowUp');
    Route::get('profile', 'ProfileController@index');

    Route::post('dashboard/chartdata', 'DashboardController@chartData');
});

Route::get('/{any}', 'SiteController@index')->where('any', '.*');

//Route::get('/home', 'HomeController@index')->name('home');
